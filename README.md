# README #

static int xioctl(int fd, int request, void *arg)

as name suggest this is for I/O control sending special request to the file.

You need to set the format image size (width x height) cropping parameter if need be

Then there are multiple ways to access the image data you can use mmap which is used here. This is fastest and most used. This maps the driver kernel memory space to user so that you can directly read from there. The image captured data.

For V4L2 capturing there are two I/O controls for continuous capturing:

VIDIOC_QBUF
and
VIDIOC_DQBUF

You can make your /dev/video0 [or any number] file non -blocking but then you have to do 
select(fd+1, &fds, NULL, NULL, &tv);
for next I/O as in async mode you don't know when your file is ready for next I/O. It may be possible that previous I/O is still in progress.

VIDIOC_STREAMON
this ioctl sets the required parameters in the camera/sensor so that sensor starts streaming.